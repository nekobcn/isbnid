#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Hypatia: Module ISBN Exception [error]
#


class ISBNError(Exception):
    pass


class ISBNRangeError(ISBNError):
    pass
