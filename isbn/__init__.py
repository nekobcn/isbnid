#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Hypatia: Module ISBN [__init__]
#

__author__ = "Neko"
__license__ = "LGPL http://www.gnu.org/licenses/lgpl.txt"
__version__ = "0.4.8"


from .isbn import ISBN
from .error import ISBNError, ISBNRangeError
