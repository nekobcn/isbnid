#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Hypatia: Module ISBN Test [test]
#

__author__ = "Neko"
__license__ = "LGPL http://www.gnu.org/licenses/lgpl.txt"
__version__ = "0.4.0"

import unittest

import isbn

# Add incorrect 979 ISBN10 Fail
# Add valisbnid digit incorrect Bookland code

isbn_ok = {
    "012345672X": [
        "012345672X",
        "9780123456724",
        "978-0-12-345672-4",
        "URN:ISBN:9780123456724",
        "10.978.012/3456724",
    ],
    "0123456789": [
        "0123456789",
        "9780123456786",
        "978-0-12-345678-6",
        "URN:ISBN:9780123456786",
        "10.978.012/3456786",
    ],
    "9780387308869": [
        "0387308865",
        "9780387308869",
        "978-0-387-30886-9",
        "URN:ISBN:9780387308869",
        "10.978.0387/308869",
    ],
    "9780393334777": [
        "0393334775",
        "9780393334777",
        "978-0-393-33477-7",
        "URN:ISBN:9780393334777",
        "10.978.0393/334777",
    ],
    "9781593273880": [
        "1593273886",
        "9781593273880",
        "978-1-59327-388-0",
        "URN:ISBN:9781593273880",
        "10.978.159327/3880",
    ],
    "9788478447749": [
        "8478447741",
        "9788478447749",
        "978-84-7844-774-9",
        "URN:ISBN:9788478447749",
        "10.978.847844/7749",
    ],
}

isbn_13 = {
    "9790123456785",
}

isbn_range = {
    "9790000000001",
}

isbn_nok = {
    "X123456781",
    "012345678X",
    "0123456780",
    "9780123456780",
    "9780123456781",
    "9790123456780",
    "9790123456781",
    "9890123456781",
    "9003434343416",
}


class ISBNTest(unittest.TestCase):
    def testISBNIn(self):
        for book in isbn_nok:
            self.assertFalse(isbn.ISBN.valid(book))
        for book in isbn_ok.keys():
            self.assertTrue(isbn.ISBN.valid(book))

    def testISBNOut(self):
        for book in sorted(isbn_ok.keys()):
            isbnid = isbn.ISBN(book)
            self.assertEqual(isbnid.isbn10(), isbn_ok[book][0])
            self.assertEqual(isbnid.isbn13(), isbn_ok[book][1])
            self.assertEqual(str(isbnid), isbn_ok[book][1])

    def testISBN13(self):
        for book in isbn_13:
            isbnid = isbn.ISBN(book)
            with self.assertRaises(isbn.ISBNError):
                isbnid.isbn10()

    def testHyphen(self):
        for book in isbn_ok.keys():
            isbnid = isbn.ISBN(book)
            self.assertEqual(isbnid.hyphen(), isbn_ok[book][2])

    def testRange(self):
        for book in isbn_range:
            isbnid = isbn.ISBN(book)
            with self.assertRaises(isbn.ISBNRangeError):
                isbnid.hyphen()

    def testURN(self):
        for book in isbn_ok.keys():
            isbnid = isbn.ISBN(book)
            self.assertEqual(isbnid.urn(), isbn_ok[book][3])

    def testDOI(self):
        for book in isbn_ok.keys():
            isbnid = isbn.ISBN(book)
            self.assertEqual(isbnid.doi(), isbn_ok[book][4])


if __name__ == "__main__":
    unittest.main()
