#!/usr/bin/env python

from setuptools import setup

from isbn import __version__

from os import path
this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='isbnid',
    version=__version__,
    author='ISBNid GitHub',
    author_email='pdfnorm@gmx.com',
    description="Python ISBN ids",
    license="GPL",
    url='https://gitlab.com/nekobcn/isbnid',
    keywords='ISBN',
    long_description=long_description,
    long_description_content_type='text/markdown',
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "Intended Audience :: Education",
        "License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: Text Processing :: Indexing",
        ],
    packages=['isbn'],
    package_dir={'isbn': 'isbn'},
    )
